/**
 * Copyright Aug 25, 2012 Edward A. Webb
 * 
 */
package com.edwardawebb.bamboo.buildbunny.service;


import org.slf4j.LoggerFactory;

import com.edwardawebb.bamboo.buildbunny.persistence.BuildBunnyRepository;
import com.edwardawebb.bamboo.buildbunny.BuildStatusReporter;
import com.edwardawebb.bamboo.buildbunny.rest.objects.BuildBunnyConfig;
import com.edwardawebb.bamboo.buildbunny.rest.objects.BunnySummary;

import java.util.UUID;

/**
 * @author Edward A. Webb
 */
public class BuildBunnyService {
	 private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(BuildBunnyService.class);

	private final BuildStatusReporter buildStatusReporter;
	private final BuildBunnyRepository buildBunnyRepository;
	
	
	
	
	public BuildBunnyService(BuildStatusReporter buildStatusReporter, BuildBunnyRepository buildBunnyRepository) {
		this.buildStatusReporter = buildStatusReporter;	
		this.buildBunnyRepository = buildBunnyRepository;
	}




	public BunnySummary getSummaryForBunny(String bunnyId) {
		BunnySummary summary = new BunnySummary(bunnyId);
					
		BuildBunnyConfig bunnyConfig = buildConfigForBunnyUsingBandana(bunnyId);
        
		if ( bunnyConfig != null){
			LOG.debug("Configuration for bunny{} loaded.",bunnyId);
			summary = buildStatusReporter.getSummaryForBunny(bunnyConfig);			
		}else{
			LOG.warn("No configuration exists for Bunny {}.", bunnyId);
		}
		
		return summary;
	}

	
	
	private BuildBunnyConfig buildConfigForBunnyUsingBandana(String bunnyId) {	
		return buildBunnyRepository.getBunny(bunnyId);
	}


	public void destroyBunny(String bunnyId) {
		 buildBunnyRepository.destroyBunny(bunnyId);
	}

	public String getUnusedBunnyId() {
		return UUID.randomUUID().toString();
	}

	public void saveBunny(BuildBunnyConfig updatedConfig) {
		buildBunnyRepository.saveBunny(updatedConfig);
	}

	public BuildBunnyConfig getBunny(String bunnyId) {
		return buildBunnyRepository.getBunny(bunnyId);
	}
}
