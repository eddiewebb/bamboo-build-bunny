package com.edwardawebb.bamboo.buildbunny.persistence.ao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.tx.Transactional;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.edwardawebb.bamboo.buildbunny.persistence.BuildBunnyRepository;
import com.edwardawebb.bamboo.buildbunny.rest.objects.BuildBunnies;
import com.edwardawebb.bamboo.buildbunny.rest.objects.BuildBunnyConfig;
import net.java.ao.ActiveObjectsException;
import net.java.ao.DBParam;
import net.java.ao.Query;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeSet;


public class BuildBunnyAoRepository implements BuildBunnyRepository {
    private final ActiveObjects ao;
    public static final String PLAN_DELIMETER = ",";

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(BuildBunnyAoRepository.class);

    public BuildBunnyAoRepository(ActiveObjects ao) {
        this.ao = ao;
    }


    @Override
    public BuildBunnyConfig getBunny(String bunnyId) {
        BuildBunnyConfigEntity[] result = ao.find(BuildBunnyConfigEntity.class, Query.select().where("UUID = ?", bunnyId));
        if (result.length == 1) {
            return fromEntity(result[0]);
        } else {
            return null;
        }
    }

    @Override
    public void saveBunny(final BuildBunnyConfig updatedConfig) {
        final BuildBunnyConfigEntity[] result = ao.find(BuildBunnyConfigEntity.class, Query.select().where("UUID = ?", updatedConfig.getBunnyId()));
        if (result.length != 1) {
            ao.executeInTransaction(new TransactionCallback<Void>() // (1)
            {
                @Override
                public Void doInTransaction() {
                    BuildBunnyConfigEntity bunny = ao.create(BuildBunnyConfigEntity.class,
                            new DBParam("UUID", updatedConfig.getBunnyId()),
                            new DBParam("NAME", updatedConfig.getName()),
                            new DBParam("DESC", updatedConfig.getDescription()),
                            new DBParam("GRP", updatedConfig.getAllowedGroup()),
                            new DBParam("FCOUNT", updatedConfig.getFailingPlansAllowedAsWarn()),
                            new DBParam("FTIME", updatedConfig.getFailingTimeAllowedAsWarn()),
                            new DBParam("EXCLUDE", updatedConfig.isExclusions()),
                            new DBParam("PLANS", StringUtils.join(updatedConfig.getPlans().toArray(), PLAN_DELIMETER))
                    );
                    return null;
                }
            });
        } else {
            ao.executeInTransaction(new TransactionCallback<Void>() // (1)
            {
                @Override
                public Void doInTransaction() {
                    BuildBunnyConfigEntity entity = result[0];
                    entity.setDescription(updatedConfig.getDescription());
                    entity.setName(updatedConfig.getName());
                    entity.setExclusions(updatedConfig.isExclusions());
                    entity.setFailingTimeAllowedAsWarn(updatedConfig.getFailingTimeAllowedAsWarn());
                    entity.setFailingPlansAllowedAsWarn(updatedConfig.getFailingPlansAllowedAsWarn());
                    entity.setPlans(StringUtils.join(updatedConfig.getPlans().toArray(), PLAN_DELIMETER));
                    entity.setAllowedGroup(updatedConfig.getAllowedGroup());
                    entity.save();
                    return null;
                }
            });
        }


    }

    @Override
    public void destroyBunny(final String bunnyId) {
        ao.executeInTransaction(new TransactionCallback<Void>() // (1)
        {
            @Override
            public Void doInTransaction() {
                ao.deleteWithSQL(BuildBunnyConfigEntity.class, "UUID = ?", bunnyId);
                return null;
            }
        });
    }

    @Override
    public BuildBunnies getAllBunnies() {
        BuildBunnyConfigEntity[] results = ao.find(BuildBunnyConfigEntity.class);
        BuildBunnies bunnies = new BuildBunnies();
        for (BuildBunnyConfigEntity entity : results) {
            bunnies.add(fromEntity(entity));
        }
        return bunnies;
    }


    private BuildBunnyConfig fromEntity(BuildBunnyConfigEntity buildBunnyConfigEntity) {
        BuildBunnyConfig config = new BuildBunnyConfig(buildBunnyConfigEntity.getUuid());
        config.setDescription(buildBunnyConfigEntity.getDescription());
        config.setExclusions(buildBunnyConfigEntity.isExclusions());
        config.setFailingPlansAllowedAsWarn(buildBunnyConfigEntity.getFailingPlansAllowedAsWarn());
        config.setFailingTimeAllowedAsWarn(buildBunnyConfigEntity.getFailingTimeAllowedAsWarn());
        config.setName(buildBunnyConfigEntity.getName());
        config.setPlans(new TreeSet<String>(Arrays.asList(StringUtils.split(buildBunnyConfigEntity.getPlans(), PLAN_DELIMETER))));
        config.setAllowedGroup(buildBunnyConfigEntity.getAllowedGroup());
        return config;

    }
}
