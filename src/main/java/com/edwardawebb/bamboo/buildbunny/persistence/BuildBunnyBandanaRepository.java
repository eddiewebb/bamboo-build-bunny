/**
 * Copyright Aug 25, 2012 Edward A. Webb
 * 
 */
package com.edwardawebb.bamboo.buildbunny.persistence;

import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import org.slf4j.LoggerFactory;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bandana.BandanaContext;
import com.atlassian.bandana.BandanaManager;
import com.edwardawebb.bamboo.buildbunny.rest.objects.BuildBunnies;
import com.edwardawebb.bamboo.buildbunny.rest.objects.BuildBunnyConfig;

/**
 * @author Edward A. Webb
 */
public class BuildBunnyBandanaRepository implements BuildBunnyRepository {
	
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(BuildBunnyRepository.class);

	private BandanaManager bandanaManager;
	/**
	 * The GLOBAL_CONTEXT provided by Bamboo.
	 */
	private static BandanaContext GLOBAL_CONTEXT=PlanAwareBandanaContext.GLOBAL_CONTEXT;
	//public static String BUNNY_SETTINGS_KEY="com.edwardawebb.buildbunny";;
	private static String BUNNY_LISTING_KEY="com.edwardawebb.buildbunny:set";
	/**
	 * A prefix to be concatenated with the bunnyId for lookups and persitence.
	 */
	private static String BUILD_BUNNY_KEY_PREFIX="com.edwardawebb.bamboo.bunny.bandana.key";
	
	
	
	
	public BuildBunnyBandanaRepository(BandanaManager bandanaManager){
		this.bandanaManager = bandanaManager;
	}
	
	
	
	private String getUniqueKeyForBunny(String bunnyId){
		return BUILD_BUNNY_KEY_PREFIX + ":" + bunnyId;
	}
	
	@Override public  BuildBunnyConfig getBunny(String bunnyId){
		BuildBunnyConfig config = null;
		Object result = bandanaManager.getValue(GLOBAL_CONTEXT, getUniqueKeyForBunny(bunnyId));
		if(null == result){
			LOG.warn("No config for bunny:{} " , bunnyId);
			return null;
		}else if (result instanceof BuildBunnyConfig){
			LOG.debug("Object is BuildBunnyCOnfig");
			try{
				config =  (BuildBunnyConfig) result;
			}catch(Exception e){
				LOG.error("Unable to cast BuildBunnyConfig",e);
			}
		}else{
			LOG.warn("Unexpected object: " + result.toString());
			return null;
		}

		return config;
	}

	@Override public void saveBunny(BuildBunnyConfig updatedConfig) {
		bandanaManager.setValue(GLOBAL_CONTEXT, getUniqueKeyForBunny(updatedConfig.getBunnyId()), updatedConfig);
		
        addBunnyId(updatedConfig.getBunnyId());
	}
	
	@Override public  void destroyBunny(String bunnyId){
		bandanaManager.removeValue(GLOBAL_CONTEXT, getUniqueKeyForBunny(bunnyId));
		removeBunnyId(bunnyId);
	}


	@Override public  BuildBunnies getAllBunnies() {
		BuildBunnies allConfiguredBunnies = new BuildBunnies();
        Set<String> bunnyIds = getAllBunnyIds();
        for (String bunnyId : bunnyIds) {
			allConfiguredBunnies.add(getBunny( bunnyId));
		}
        return allConfiguredBunnies;
	}

	private  void addBunnyId(String bunnyId) {
		 Set<String> bunnyIds = getAllBunnyIds();
		 if(null == bunnyIds){
			 bunnyIds = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
		 }
		 bunnyIds.add(bunnyId);
		 bandanaManager.setValue(GLOBAL_CONTEXT,BUNNY_LISTING_KEY,bunnyIds);
	}

	private  void removeBunnyId(String bunnyId) {
		 Set<String> bunnyIds = getAllBunnyIds();
		 if(null == bunnyIds){
			 bunnyIds = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
		 }
		 bunnyIds.remove(bunnyId);
		 bandanaManager.setValue(GLOBAL_CONTEXT,BUNNY_LISTING_KEY,bunnyIds);
	}

	private  Set<String> getAllBunnyIds() {
		return (TreeSet<String>)bandanaManager.getValue(GLOBAL_CONTEXT,BUNNY_LISTING_KEY);
	}

}
