package com.edwardawebb.bamboo.buildbunny.persistence;

import com.edwardawebb.bamboo.buildbunny.rest.objects.BuildBunnies;
import com.edwardawebb.bamboo.buildbunny.rest.objects.BuildBunnyConfig;


public interface BuildBunnyRepository {

    BuildBunnyConfig getBunny(String bunnyId);

    void saveBunny(BuildBunnyConfig updatedConfig);

    void destroyBunny(String bunnyId);


    BuildBunnies getAllBunnies();
}
