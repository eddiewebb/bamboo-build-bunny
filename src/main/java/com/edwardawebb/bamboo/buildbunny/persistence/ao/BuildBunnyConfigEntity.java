package com.edwardawebb.bamboo.buildbunny.persistence.ao;

import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.Mutator;
import net.java.ao.Preload;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;
import net.java.ao.schema.Unique;



@Table("BUNNY")
@Preload
public interface BuildBunnyConfigEntity  extends Entity {

    @Unique
    @Mutator("UUID")
    void setUuid(String config);
    @Accessor("UUID")
    String getUuid();


    @Mutator("NAME")
    void setName(String name);
    @Accessor("NAME")
    String getName();

    @Mutator("DESC")
    void setDescription(String description);
    @Accessor("DESC")
    String getDescription();


    @Mutator("GRP")
    void setAllowedGroup(String group);
    @Accessor("GRP")
    String getAllowedGroup();



    @Accessor("EXCLUDE")
    boolean isExclusions();
    @Mutator("EXCLUDE")
    void setExclusions(boolean exclusions);

    @Accessor("PLANS")
    /**
     * Comma seperated list of Bamboo Plan Keys PROJ-PLANA,PROJ-PLANB,etc
     * (ao does not support persisting collections)
     */
    String getPlans();
    @Mutator("PLANS")
    @StringLength(value=StringLength.UNLIMITED)
    void setPlans(String plans);


    @Accessor("FCOUNT")
    int getFailingPlansAllowedAsWarn();
    @Mutator("FCOUNT")
    void setFailingPlansAllowedAsWarn(int failingPlansAllowedAsWarn);

    @Accessor("FTIME")
    long getFailingTimeAllowedAsWarn();
    @Mutator("FTIME")
    void setFailingTimeAllowedAsWarn(long failingTimeAllowedAsWarn) ;



}
