/**
 * 
 */
package com.edwardawebb.bamboo.buildbunny.rest.anon;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.LoggerFactory;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.edwardawebb.bamboo.buildbunny.rest.objects.BunnySummary;
import com.edwardawebb.bamboo.buildbunny.rest.objects.HelpBunny;
import com.edwardawebb.bamboo.buildbunny.service.BuildBunnyService;
/**
 * This class represents the endpoint used to access {@link BunnySummary} resources.
 * @author Eddie Webb
 *
 */
@Path("summary")
public class BunnySummaryResource {
	
	 private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(BunnySummaryResource.class);

	
	public static final String PARAM_BUNNY_ID = "bunnyId";
	
	
	private BuildBunnyService buildBunnyService;

	BunnySummaryResource(BuildBunnyService buildBunnyService){
		this.buildBunnyService = buildBunnyService;		
	}
	
	
	
	 /**
     * This method will be called if a user calls the summary resource without an ID in the path
     * @return a helpful message
     */
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getMessage()
    {
        return Response.ok(new HelpBunny()).build();
    }
 
    /**
     * Lookup and return the Summary for this Bunny, based on ID
     * @param bunnyId the ID of the bunny's config
     * @return the BunnySummary for all configured plans
     */
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/{bunnyId}")
    public Response getMessageFromPath(@PathParam(PARAM_BUNNY_ID) String bunnyId)
    {
        long startTime = System.currentTimeMillis();
    	LOG.debug("BuildBunny: {} making summary request." , bunnyId);
		BunnySummary  summary = buildBunnyService.getSummaryForBunny(bunnyId);
        long runTime = System.currentTimeMillis() - startTime;
        summary.setTimeToEvaluate(runTime);
        if(LOG.isDebugEnabled()){
            LOG.debug("BuildBunny: {} finished request in {} ms" , bunnyId, runTime);
        }
		return Response.ok(summary).build();
    	
    }



    
    


    
    
	
    
    
    
    
    
    
}
