/**
 * Copyright Aug 25, 2012 Edward A. Webb
 * 
 */
package com.edwardawebb.bamboo.buildbunny.rest.objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.edwardawebb.bamboo.buildbunny.rest.anon.BunnySummaryResource;

/**
 * Represents tha collective status of builds defined for this request. Served by {@link BunnySummaryResource}
 * 
 * See README.md for how these map to a response
 * 
 * @author Edward A. Webb
 */
@XmlRootElement(name = "help")
@XmlAccessorType(XmlAccessType.FIELD)
public class HelpBunny implements ReturnableBunny {
	
	
	/*
	 * Top level elements
	 */
  
    @XmlElement(name = "message")
    private String message = "You must provide a BuildBunny ID as part of the path (/rest/build-bunny/1.0/summary/123.json).  This ID should be configured in the Admin Section of bamboo";
        
   
 
    public HelpBunny() {    	
        
        
    }


 
   
}
