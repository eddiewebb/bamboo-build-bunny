/**
 * Copyright Aug 25, 2012 Edward A. Webb
 * 
 */
package com.edwardawebb.bamboo.buildbunny.rest.objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.edwardawebb.bamboo.buildbunny.rest.anon.BunnySummaryResource;

/**
 * Represents tha collective status of builds defined for this request. Served by {@link BunnySummaryResource}
 * 
 * See README.md for how these map to a response
 * 
 * @author Edward A. Webb
 */
@XmlRootElement(name = "error")
@XmlAccessorType(XmlAccessType.FIELD)
public class ErrorBunny  implements ReturnableBunny {
	
	/*
	 * Top level elements
	 */
  
    @XmlElement(name = "message")
    private String message ;
        
   
 
    public ErrorBunny(String message) {    	
        this.message=message;
        
    }
 
   
}
