/**
 * Copyright Aug 25, 2012 Edward A. Webb
 * 
 */
package com.edwardawebb.bamboo.buildbunny.rest.objects;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.edwardawebb.bamboo.buildbunny.rest.anon.BunnySummaryResource;

/**
 * Represents tha collective status of builds defined for this request. Served by {@link BunnySummaryResource}
 * 
 * See README.md for how these map to a response
 * 
 * @author Edward A. Webb
 */
@XmlRootElement(name = "Summary")
@XmlAccessorType(XmlAccessType.FIELD)
public class BunnySummary  implements ReturnableBunny {
	@XmlAttribute
    private String bunnyId;
	@XmlAttribute
    private String name;
    @XmlAttribute
    private String description;

	
    //@XmlElement(name = "status")
    private StatusLevel status;

    @XmlElementWrapper(name = "offenders",nillable=true)
    @XmlElement(name="offender",type=Offender.class,nillable=true)
    private List<Offender> offenders  ;


	@XmlAttribute
	private long timeToEvaluate=0;
   
 
    public BunnySummary() {
    }
 
    public BunnySummary(String bunnyId) {
        this.bunnyId = bunnyId;     
    }
    
 
   

	public String getBunnyId() {
		return bunnyId;
	}



	
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public StatusLevel getStatus() {
		return status;
	}

	public void setStatus(StatusLevel status) {
		this.status = status;
	}

	public List<Offender> getOffenders() {
		return offenders;
	}

	public void setOffenders(List<Offender> offenders) {
		this.offenders = offenders;
	}


	public long getTimeToEvaluate() {
		return timeToEvaluate;
	}

	public void setTimeToEvaluate(long timeToEvaluate) {
		this.timeToEvaluate = timeToEvaluate;
	}

	public enum StatusLevel{
    	OK("all plans passing"), WARN("Some failures"), FAIL("Exceeding failures");
    	
    	private String value;
    	
    	StatusLevel(String value){
    		this.value=value;
    	}
    	
    	String value(){
    		return value;
    	}
    	
    }
    
    
    
    
}
