/**
 * Copyright Aug 25, 2012 Edward A. Webb
 */
package com.edwardawebb.bamboo.buildbunny.rest.objects;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.edwardawebb.bamboo.buildbunny.persistence.ao.BuildBunnyConfigEntity;
import org.slf4j.LoggerFactory;

import com.edwardawebb.bamboo.buildbunny.service.BuildBunnyService;

/**
 * Defines the method for reporting to a given Build Bunny
 * Should the list of plans be checked, or should all plans be checked, excluding the list.
 * Should the default 1 plan, 10 minute warning be altered
 *
 * @author Edward A. Webb
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BuildBunnyConfig implements Serializable {
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(BuildBunnyConfig.class);


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String bunnyId; //also uses as key to store this object, a UUID

    private String name = "unamed Bunny";
    private String description = "bunny Described";
    private String allowedGroup = "ADD A GROUP YOU BELONG TO!!!";
    private boolean exclusions = false; //by default we use the inclusion list
    Set<String> plans = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);

    private int failingPlansAllowedAsWarn = 1;
    private long failingTimeAllowedAsWarn = 600000; //ten minutes


    public BuildBunnyConfig() {

    }

    public BuildBunnyConfig(String bunnyId) {
        this.bunnyId = bunnyId;
    }


    public boolean isMonitoringPlan(String planKey) {
        return plans.contains(planKey);
    }

    public String getBunnyId() {
        return bunnyId;
    }

    public void setBunnyId(String bunnyId) {
        this.bunnyId = bunnyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAllowedGroup() {
        return allowedGroup;
    }

    public void setAllowedGroup(String allowedGroup) {
        this.allowedGroup = allowedGroup;
    }

    public boolean isExclusions() {
        return exclusions;
    }

    public void setExclusions(boolean exclusions) {
        this.exclusions = exclusions;
    }

    public Set<String> getPlans() {
        return plans;
    }

    public void setPlans(Set<String> plans) {
        this.plans = plans;
    }


    public int getFailingPlansAllowedAsWarn() {
        return failingPlansAllowedAsWarn;
    }

    public void setFailingPlansAllowedAsWarn(int failingPlansAllowedAsWarn) {
        this.failingPlansAllowedAsWarn = failingPlansAllowedAsWarn;
    }

    public long getFailingTimeAllowedAsWarn() {
        return failingTimeAllowedAsWarn;
    }

    public void setFailingTimeAllowedAsWarn(long failingTimeAllowedAsWarn) {
        this.failingTimeAllowedAsWarn = failingTimeAllowedAsWarn;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((bunnyId == null) ? 0 : bunnyId.hashCode());
        result = prime * result
                + ((description == null) ? 0 : description.hashCode());
        result = prime * result + (exclusions ? 1231 : 1237);
        result = prime * result + failingPlansAllowedAsWarn;
        result = prime
                * result
                + (int) (failingTimeAllowedAsWarn ^ (failingTimeAllowedAsWarn >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((plans == null) ? 0 : plans.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BuildBunnyConfig other = (BuildBunnyConfig) obj;
        if (bunnyId == null) {
            if (other.bunnyId != null)
                return false;
        } else if (!bunnyId.equals(other.bunnyId))
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (exclusions != other.exclusions)
            return false;
        if (failingPlansAllowedAsWarn != other.failingPlansAllowedAsWarn)
            return false;
        if (failingTimeAllowedAsWarn != other.failingTimeAllowedAsWarn)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (plans == null) {
            if (other.plans != null)
                return false;
        } else if (!plans.equals(other.plans))
            return false;
        return true;
    }


}