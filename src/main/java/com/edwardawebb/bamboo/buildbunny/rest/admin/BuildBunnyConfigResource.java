package com.edwardawebb.bamboo.buildbunny.rest.admin;

import java.io.IOException;
import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.edwardawebb.bamboo.buildbunny.rest.objects.BuildBunnyConfig;
import org.slf4j.LoggerFactory;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserManager;
import com.edwardawebb.bamboo.buildbunny.persistence.BuildBunnyRepository;
import com.edwardawebb.bamboo.buildbunny.rest.objects.BuildBunnies;
import com.edwardawebb.bamboo.buildbunny.service.BuildBunnyService;

@Path("admin")
public class BuildBunnyConfigResource {
	private final UserManager userManager;
	private final TransactionTemplate transactionTemplate;
	private final BandanaManager bandanaManager;
	private final LoginUriProvider loginUriProvider;
	private final BuildBunnyRepository buildBunnyRepository;
	private static final org.slf4j.Logger LOG = LoggerFactory
			.getLogger(BuildBunnyService.class);

	public BuildBunnyConfigResource(UserManager userManager,
			BandanaManager bandanaManager,
			TransactionTemplate transactionTemplate,
			LoginUriProvider loginUriProvider,
			BuildBunnyRepository buildBunnyRepository) {
		this.userManager = userManager;
		this.bandanaManager = bandanaManager;
		this.transactionTemplate = transactionTemplate;
		this.loginUriProvider = loginUriProvider;
		this.buildBunnyRepository = buildBunnyRepository;
	}

	/**
	 * Called by initial display of Admin form. Presents a list of all
	 * configured build bunnies.
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response get(@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws IOException {
		try {
			final String username = userManager.getRemoteUsername(request);
			if (username == null ) {
				redirectToLogin(request, response);
				return null;
			}else{

			}

			return Response.ok(
					transactionTemplate
							.execute(new TransactionCallback<Object>() {
								public Object doInTransaction() {

									BuildBunnies allConfiguredBunnies = buildBunnyRepository
											.getAllBunnies();
									BuildBunnies returnedBunnies = new BuildBunnies();

									if (!userManager.isSystemAdmin(username)) {
										for (BuildBunnyConfig bunny : allConfiguredBunnies.getBunnies()) {
											if (null != bunny.getAllowedGroup() && userManager.isUserInGroup(username, bunny.getAllowedGroup())) {
												returnedBunnies.add(bunny);
											}else{
												LOG.debug("USer {} is not in group {} specified by bunny; removing bunny from results",username,bunny.getAllowedGroup());
											}
										}
									}else{
										returnedBunnies.setBunnies(allConfiguredBunnies.getBunnies());
									}

									return returnedBunnies;
								}
							})).build();
		} catch (Exception e) {
			LOG.error("Error occured hitting admin RESt endpoint", e);

		}
		return Response.noContent().build();
	}

	private void redirectToLogin(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		response.sendRedirect(loginUriProvider.getLoginUri(getUri(request))
				.toASCIIString());
	}

	private URI getUri(HttpServletRequest request) {
		StringBuffer builder = request.getRequestURL();
		if (request.getQueryString() != null) {
			builder.append("?");
			builder.append(request.getQueryString());
		}
		return URI.create(builder.toString());
	}
}