/**
 * Copyright Aug 27, 2012 Edward A. Webb
 * 
 */
package com.edwardawebb.bamboo.buildbunny.rest.objects;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Edward A. Webb
 */
@XmlRootElement(name="BuildBunnies")
@XmlAccessorType(XmlAccessType.FIELD)
public class BuildBunnies {

    @XmlElementWrapper(name = "bunnyConfigs",nillable=true)
    @XmlElement(name="bunnyConfig",type=BuildBunnyConfig.class,nillable=true)
	private List<BuildBunnyConfig> bunnies=new ArrayList<BuildBunnyConfig>();

	public void add(BuildBunnyConfig bunny) {
		this.bunnies.add(bunny);		
	}

	public List<BuildBunnyConfig> getBunnies() {
		return bunnies;
	}

	public void setBunnies(List<BuildBunnyConfig> bunnies) {
		this.bunnies = bunnies;
	}
	
	
	
	
}
