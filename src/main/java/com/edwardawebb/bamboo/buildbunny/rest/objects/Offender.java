package com.edwardawebb.bamboo.buildbunny.rest.objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Offender{    	
	    //@XmlElement(name = "devName")
	    private String devName;
	    
	    //@XmlElement(name = "planName")
	    private String planName;
	    
	   // @XmlElement(name = "failTime")
	    private String failTime;
	        	    
	    
		public Offender() {
			
		}


		public Offender(String devName, String planName, String failTime) {				
			this.devName = devName;
			this.planName = planName;
			this.failTime = failTime;
		}


		public String getDevName() {
			return devName;
		}


		public void setDevName(String devName) {
			this.devName = devName;
		}


		public String getPlanName() {
			return planName;
		}


		public void setPlanName(String planName) {
			this.planName = planName;
		}


		public String getFailTime() {
			return failTime;
		}


		public void setFailTime(String failTime) {
			this.failTime = failTime;
		}
		
		
		
}	
		
