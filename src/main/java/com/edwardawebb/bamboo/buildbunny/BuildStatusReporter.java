/**
 * Copyright Aug 25, 2012 Edward A. Webb
 * 
 */
package com.edwardawebb.bamboo.buildbunny;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.TopLevelPlan;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.plan.cache.ImmutableTopLevelPlan;
import com.atlassian.bamboo.resultsummary.ImmutableResultsSummary;
import org.joda.time.DateTime;
import org.joda.time.Instant;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormat;
import org.slf4j.LoggerFactory;

import com.atlassian.bamboo.v2.build.trigger.TriggerReason;
import com.edwardawebb.bamboo.buildbunny.rest.objects.BuildBunnyConfig;
import com.edwardawebb.bamboo.buildbunny.rest.objects.BunnySummary;
import com.edwardawebb.bamboo.buildbunny.rest.objects.BunnySummary.StatusLevel;
import com.edwardawebb.bamboo.buildbunny.rest.objects.Offender;

/**
 * @author Edward A. Webb
 */
public class BuildStatusReporter {

	
	 private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(BuildStatusReporter.class);

	
	 
	private CachedPlanManager planManager;

	
	// TODO: Add once upgraded to 4.2 
	// private TrackingEntryManager trackingManagerEntry;
	// will allow us to get tracking entry for a build, and from there a list of all responsible persons.
	


	public BunnySummary getSummaryForBunny(BuildBunnyConfig config){
		BunnySummary summary = new BunnySummary(config.getBunnyId());
		summary.setName(config.getName());
		summary.setDescription(config.getDescription());
		try{

			List<ImmutableTopLevelPlan>availablePlans = new ArrayList<ImmutableTopLevelPlan>();
			long start = System.currentTimeMillis();
			Set<String> plansWeCareAbout = config.getPlans();
			for (String planKeyString : plansWeCareAbout){
				availablePlans.add(planManager.getPlanByKey(PlanKeys.getPlanKey(planKeyString),ImmutableTopLevelPlan.class));
			}
			long stop = System.currentTimeMillis();
			LOG.debug("Selecting  plans to inspect from planmanager took {} ms", stop-start);
			
			boolean maxIntervalExceed =false;
			List<Offender> offenders = new ArrayList<Offender>();
			for (ImmutableTopLevelPlan topLevelPlan : availablePlans) {
				//only check plans we care about
				if(!config.isExclusions() == config.getPlans().contains(topLevelPlan.getPlanKey().getKey())){
					LOG.debug("Evaluating Plan {}",topLevelPlan);
					if(null != topLevelPlan.getCurrentStatus() && topLevelPlan.getCurrentStatus().equals(ImmutablePlan.STATUS_FAIL)){
						LOG.debug("Plan {} is a Failure",topLevelPlan);
						ImmutableResultsSummary lastFinishedResult = topLevelPlan.getLatestResultsSummary();

						Period timeSinceFailure = new Period(new DateTime(lastFinishedResult.getBuildCompletedDate()),new Instant());
						if(timeSinceFailure.getMillis() > config.getFailingTimeAllowedAsWarn()){
							maxIntervalExceed = true;
						}
						Offender offender = new Offender();
						offender.setPlanName(topLevelPlan.getName());
						offender.setDevName(lastFinishedResult.getReasonSummary());
						offender.setFailTime(timeSinceFailure.toString(PeriodFormat.wordBased()));
						offenders.add(offender);
					}				
				}
			}
			summary.setOffenders(offenders);
			
			/*
			 * Threshold status
			 */
			if(offenders.isEmpty()){
				LOG.debug("All Plans OK");
				summary.setStatus(StatusLevel.OK);
			}else if(offenders.size() > config.getFailingPlansAllowedAsWarn() || maxIntervalExceed){	
				LOG.debug("Things are one fire !!");
				summary.setStatus(StatusLevel.FAIL);
			}else{
				LOG.debug("Some plans are Warning");
				summary.setStatus(StatusLevel.WARN);
			}
		}catch(Exception e){
			LOG.error("Error caught while processing buukd summaries.",e);
		}
		
		
		return summary;
		
	}
	
	




	public void setPlanManager(final CachedPlanManager planManager) {
		this.planManager = planManager;
	}
	
	
}
