/**
 * Copyright Aug 26, 2012 Edward A. Webb
 * 
 */
package com.edwardawebb.bamboo.buildbunny.admin;

import java.io.IOException;
import java.net.URI;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edwardawebb.bamboo.buildbunny.service.BuildBunnyService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;

import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.TopLevelPlan;
import com.atlassian.bamboo.project.Project;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.RenderingException;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.edwardawebb.bamboo.buildbunny.rest.admin.BuildBunnyConfigResource;
import com.edwardawebb.bamboo.buildbunny.rest.anon.BunnySummaryResource;
import com.edwardawebb.bamboo.buildbunny.rest.objects.BuildBunnies;
import com.edwardawebb.bamboo.buildbunny.rest.objects.BuildBunnyConfig;

/**
 * This class presents the UI in the admin area of bamboo.  
 * It in turns using JS to make async calls to {@link BuildBunnyConfigResource} (exposed on the URL configured in atlassian-plugin.xml),
 * which returns a {@link BuildBunnies} object as JSON to the UI.
 * 
 * @author Edward A. Webb
 */
public class AdminServlet extends HttpServlet {
	public static String ACTION_EDIT = "edit";
	public static String ACTION_ADD = "add";
	public static String ACTION_DELETE = "delete";
    public static int MAX_PLANS_NON_ADMIN = 50;
	 private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(AdminServlet.class);

	private final UserManager userManager;
    private final TemplateRenderer renderer;
    private final LoginUriProvider loginUriProvider;
	private final PlanManager planManager;
	private final BuildBunnyService buildBunnyService;
 
    public AdminServlet(UserManager userManager, LoginUriProvider loginUriProvider, TemplateRenderer renderer,PlanManager planManager,BuildBunnyService buildBunnyService)
    {
        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
        this.renderer = renderer;
        this.planManager = planManager;
        this.buildBunnyService = buildBunnyService;
    }
 
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        String username = userManager.getRemoteUsername(request);
        if (username == null )
		{
			  redirectToLogin(request, response);
			  return;
		}
        
        response.setContentType("text/html;charset=utf-8");
        String action = request.getParameter("action");
        if(!StringUtils.isEmpty(action)){
    		String bunnyId = request.getParameter("id");
    		if(ACTION_EDIT.equalsIgnoreCase(action) && !StringUtils.isEmpty(bunnyId)){
        		generateEditPageForBunny(bunnyId,response,username);
        	}else if(ACTION_DELETE.equalsIgnoreCase(action) && !StringUtils.isEmpty(bunnyId)){
        		if(deleteBunny(request,username)){
                    response.sendRedirect(request.getRequestURI());
                }
        	}else if(ACTION_ADD.equalsIgnoreCase(action)){
        		String newBunnyId = buildBunnyService.getUnusedBunnyId();
        		generateEditPageForBunny(newBunnyId,response,username);
        	}else{
        		generateUnsupportedOperation(response);
        	}
        }else{
            renderer.render("templates/adminSummary.vm", response.getWriter());        	
        }
    }
    

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
    	String username = userManager.getRemoteUsername(request);
        if (username == null) {
            redirectToLogin(request, response);
              return;
        }
        
        response.setContentType("text/html;charset=utf-8");
        String action = request.getParameter("action");
        if(!StringUtils.isEmpty(action)){
    		String bunnyId = request.getParameter("id");
    		LOG.info("processing bunny {}",bunnyId);
    		if(ACTION_EDIT.equalsIgnoreCase(action) && !StringUtils.isEmpty(bunnyId)){
    			if(saveEditedBunny(request,username)){
        			response.sendRedirect(request.getRequestURI());
        		}else{
        			response.sendRedirect(request.getHeader("Referer"));
        		}
        	}else if(ACTION_ADD.equalsIgnoreCase(action) && !StringUtils.isEmpty(bunnyId)){
                if(saveEditedBunny(request,username)){
                    response.sendRedirect(request.getRequestURI());
                }else{
                    response.sendRedirect(request.getHeader("Referer"));
                }
            }else if(ACTION_DELETE.equalsIgnoreCase(action) && !StringUtils.isEmpty(bunnyId)){
                if(deleteBunny(request, username)){
                    response.sendRedirect(request.getRequestURI());
                }else{
                    response.sendRedirect(request.getHeader("Referer"));
                }
            }else{
        		generateUnsupportedOperation(response);
        	}
        }else{
        	LOG.info("invalid post");
            renderer.render("templates/adminSummary.vm", response.getWriter());        	
        }
    }

    private boolean deleteBunny(HttpServletRequest request, String username) {
        String bunnyId = request.getParameter("id");
        BuildBunnyConfig updatedConfig = buildBunnyService.getBunny(bunnyId);
        if( null == updatedConfig){
            return false;//no such bunny
        }else{
            //existing bunny, make sure they are allowed
            if( ! userManager.isSystemAdmin(username) ){
                if(null == updatedConfig.getAllowedGroup() || ! userManager.isUserInGroup(username,updatedConfig.getAllowedGroup())) {
                    LOG.warn("Invalid user attempting to save existing bunny");
                    //not allowed !
                    return false;
                }
            }
            buildBunnyService.destroyBunny(bunnyId);
            return true;
        }
    }


    private boolean saveEditedBunny(HttpServletRequest request, String username) {
		Map<String,String> postedData = request.getParameterMap();
		
		String bunnyId = request.getParameter("id");
		boolean isExlusions = postedData.containsKey("isExclusion");
		BuildBunnyConfig updatedConfig = buildBunnyService.getBunny(bunnyId);
		if( null == updatedConfig){
			//new bunny, trust anyone
			updatedConfig = new BuildBunnyConfig(bunnyId);
		}else{
			//existing bunny, make sure they are allowed
			if( ! userManager.isSystemAdmin(username) ){
              if(null == updatedConfig.getAllowedGroup() || ! userManager.isUserInGroup(username,updatedConfig.getAllowedGroup())) {
                  LOG.warn("Invalid user attempting to save existing bunny");
                  //not allowed !
                  return false;
              }
			}
		}
		updatedConfig.setExclusions(isExlusions);
		updatedConfig.setName(request.getParameter("bunnyName"));
		updatedConfig.setDescription(request.getParameter("bunnyDescription"));
		updatedConfig.setFailingPlansAllowedAsWarn(Integer.parseInt(request.getParameter("bunnyAllowedPlans")));
		updatedConfig.setFailingTimeAllowedAsWarn(Integer.parseInt(request.getParameter("bunnyAllowedTime")));
		updatedConfig.setAllowedGroup(request.getParameter("allowedGroup"));
		if(postedData.containsKey("planKeys")){
			Set<String> plans = new TreeSet<String>(Arrays.asList(request.getParameterValues("planKeys")));
            if( ! userManager.isSystemAdmin(username) && plans.size() > MAX_PLANS_NON_ADMIN ){
                LOG.warn("Non Admins may not set a bunny with more then {} plans",MAX_PLANS_NON_ADMIN);
                return false;
            }
			LOG.info("Bunny {} has {} plans to monitor",bunnyId,plans.size());
			updatedConfig.setPlans(plans);
		}else if(!isExlusions){
			return false;
		}
		buildBunnyService.saveBunny(updatedConfig);
        return true;
	}

	private void generateUnsupportedOperation(HttpServletResponse response) {
		// FIXME
		LOG.error("Unsupported Build BunnY Action");
	}

	private void generateEditPageForBunny(String bunnyId,
			HttpServletResponse response,String username) throws RenderingException, IOException {

		Map<String,Object> bunnyUpdateObjects = loadBunnyAndPlans(bunnyId);

		renderer.render("templates/adminEdit.vm", bunnyUpdateObjects,response.getWriter());
	}

	/**
	 * Create a map of all plans to populate velocity template.
	 * @return
	 */
	private Map<String, Object> loadBunnyAndPlans(String bunnyId) {
		Map<String,Object> template = new HashMap<String,Object>();
		
		BuildBunnyConfig config = buildBunnyService.getBunny(bunnyId);
		
		if(null == config){
			template.put("bunny", new BuildBunnyConfig(bunnyId));
		}else{
			template.put("bunny",buildBunnyService.getBunny(bunnyId));
		}
		template.put("projects", loadAllPlansByProject());
		
		return template;
	}

	private Map<Project, Collection<TopLevelPlan>> loadAllPlansByProject() {
		
		return planManager.getProjectPlanMap(TopLevelPlan.class, false);
	}

	private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }
  
    private URI getUri(HttpServletRequest request)
    {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null)
        {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }
}
