# Using Build Bunny with bamboo-blinker
bamboo-blinker is a executable written in go to interact with build bunny and send results to a USB Blync Light

You can download pre-compiled versions available for MacOSx https://github.com/davidehringer/bamboo-blinker/releases for simple plug-n-go support

or clone the source code and modify for your needs https://github.com/davidehringer/bamboo-blinker.

