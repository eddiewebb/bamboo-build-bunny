param (
    [string]$bunny = $(throw "-bunny parameter with status endpoint is required."),
    [int]$blync = 0,
    [int]$interval = 300
 )




function print-offenders($status)
{
    Write-Host "WARN: The following plans are failing:"
    foreach ($offender in $status.offenders)
    {
        $offender.planName
        $offender.devName
        Write-Host ""
    }
}



Write-Host "INFO: Using Bunny endpoint $bunny at interval of $interval and outputting to blync instance $blync"
$pwd = (get-Item -Path ".\" -Verbose).FullName
[Reflection.Assembly]::LoadFile( $pwd + "\Blynclight.dll")
$bc = new-object Blynclight.BlynclightController
$bc.initBlyncDevices()
$bc.TurnOnGreenLight($blync)

$previousStatus = "OK"
while(1){
    $status = Invoke-WebRequest -Uri $bunny | ConvertFrom-Json

    Write-Host "Build Bunny reports status: " + $status.status
    if( $status.status -ne $previousStatus){
        $previousStatus = $status.status

        $bc.StopLightFlash($blync)
        $bc.StopMusicPlay($blync)


        if ( $status.status -eq "FAIL" )
        {
            $bc.ResetLight($blync)
            $bc.TurnOnRedLight($blync)
            $bc.SelectLightFlashSpeed($blync,3)
            $bc.StartLightFlash($blync)
            $bc.SetMusicVolume($blync,0)
            $bc.SelectMusicToPlay($blync,8)
            $bc.StartMusicPlay($blync)
            print-offenders($status)
        }elseif ( $status.status -eq "WARN"){
            $bc.ResetLight($blync)
            $bc.TurnOnOrangeLight($blync)
            $bc.SelectLightFlashSpeed($blync,3)
            $bc.StartLightFlash($blync)
            print-offenders($status)
        } else{
            $bc.ResetLight($blync)
            $bc.TurnOnGreenLight($blync)
            $bc.SelectMusicToPlay($blync,6)
            $bc.StartMusicPlay($blync)
        }
    }else{
        Write-Host "no change"
    }
    start-sleep -seconds $interval
}
