# BlyncLight with Windows Powershell

## Install / Setup
1. Download Blynclight SDK from http://blynclight.proboards.com/attachment/download/8 (need account)
1. Extract the appropriate .dll to a folder on your machine
1. Place blynclight.bat and blynclight.ps1 *next to* the .dll (same folder)
1. Adjust colors, loop time, and music choice as desired
TIP: Use the .exe included in the SDK to play with samples.

## Use / Running
Do to Windows security you'll need to execute the .bat file, which invokes the powershell script.

You can add a shortcut to the .bat from the windows start menu > startup folder to always run on startup.