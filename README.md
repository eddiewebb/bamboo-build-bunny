# Bamboo Build Bunny

This plugin provides 2 primary features:
1. A admin screen to configure a collection of plans that a given Build Bunny should care about
1. A REST service to expose a short and sweet report on those plans.



The endpoint to test for JSON is http://localhost:6990/bamboo/rest/build-bunny/1.0/summary/123  where 123 is the unique bunny id.

## Admin Config

The admin config allows 1-n bunnies to be defined. For each bunny you may define:
- The list of plans to check on each query
- The thresholds for WARN and FLAMES
 - Based on Time and Count
   WARN if a plan is failing, BUT total count of failing plans < X AND no time exceeds Y
   Otherwise FLAMES



## REST API
Possible Return values as JSON or XML:

```Summary
    |- Status = {OK,WARN,FLAMES}
    |- Offenders (repeats for each failing build)
        |- DevName = <developer name>
        |- PlanName = <failing plan name> 
        `- FailTime = <length since failure>
```
        
```{
    bunnyId: "124",
    status: "WARN",
    offenders: [
        {
            devName: "Manual build by <a href="http://Media3Center:6990/bamboo/browse/user/admin">Joey Bag'oDonuts</a>",
            planName: "Alpha Project - Alpha Plan",
            failTime: "1 hour, 33 minutes, 55 seconds and 952 milliseconds"
        }
    ]
}```
    
## Example Clients


Can be used with [Blync Lights](http://www.embrava.com/products/blync-light?variant=328886579) for lights and sound, [cheaper USB LEDS likeblink(1)](http://buy.thingm.com/blink1) without sound, or , Arduinos, Rasberry PI, etc.  See [Samples](/src/samples)